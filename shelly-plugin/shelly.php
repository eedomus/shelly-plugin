<?

    //*************************************************************************************
    //
    // Plugin Shelly
    // by KikiFUNstyle (^_^)
    //
    //**************************************************************************************
    
    $type = getArg('t', false);    

    // arg periphId 
    $periphId = getArg("periphId", false);
    // periph caract
    $periphIdCaractdUrl = 'http://localhost/api/get?action=periph.caract&periph_id='.$periphId.'&show_config=1&format=xml';
    $periphIdCaractdUrlXml = httpQuery($periphIdCaractdUrl);     
    // patch '|' on xml node
    $periphIdCaractdUrlXml = str_replace('hide|', 'hide_', $periphIdCaractdUrlXml);


    $actionArg = getArg('action', false);

    if($actionArg == 'init' || $actionArg == 'check') {
        // [VAR1]
        // _username_:_password_@ipShelly
        $identificationShelly = xpath($periphIdCaractdUrlXml, '//VAR1');


        $xml .= '<result>';
        
        // [VAR2]
        // example : 12345,678910;0;out_on_url,out_off_url
        // action url name (to contruct /setting/actions [urls])
        // [periphIdList];[index];[urlName1,urlName2]
        $var2 = xpath($periphIdCaractdUrlXml, '//VAR2');
        
        $var2Explode = explode(';', $var2);
        // periphId list to set new value FROM shelly action url
        $periphIdList = $var2Explode[0];
        // index of action url
        $indexList = explode(',', $var2Explode[1]);
        // action url list of index
        $actionUrlList = explode(',', $var2Explode[2]);

        // IP Eedomus
        $ipEedomus = sdk_get_ip_from_ip_or_mac('localhost');
        
        $urlActionUrl = 'http://'.$ipEedomus.'/script/?exec=shelly.php%26periphId='.$periphIdList;
        
        $xml .= '<init>';
        
        $isAllSuccess = 1;
        // call /settings/actions to set actionUrl on each specific periphId
        foreach ($indexList as $index) {
            $xml .= '<index_'.$index.'>';
            foreach ($actionUrlList as $actionUrl) {
                $xml .= '<'.$actionUrl.'>';   
                
                $url = 'http://'.$identificationShelly.'/settings/actions?index='.$index.'&name='.$actionUrl.'&enabled=true&urls[]='.$urlActionUrl;
                
                // call url to set url action with Shelly API
                if($actionArg == 'init') {
                    $urlResponse = httpQuery($url);
                    $xml .= '<urlReponse><![CDATA['.$urlResponse.']]></urlReponse>';
                }
                
                $actionShelly = httpQuery('http://'.$identificationShelly.'/settings/actions');
                $actionUrlResponse = jsonToXML($actionShelly);
                
                $actionUrls = xpath($actionUrlResponse, '//'.$actionUrl.'/btn_on_url/urls');
                $actionUrls = str_replace('&', '%26', $actionUrls);
                $isEnabled = xpath($actionUrlResponse, '//'.$actionUrl.'/btn_on_url/enabled');
                
                if($urlActionUrl == $actionUrls && $isEnabled == 1) {
                    $xml .= '<success>1</success>';
                } else {
                    $xml .= '<success>0</success>';
                    $isAllSuccess = 0;
                }
                // XML
                $xml .= '<set_url><![CDATA['.$url.']]></set_url>';
                $xml .= '<action_url><![CDATA['.$urlActionUrl.']]></action_url>';
                $xml .= '<url_shelly_found><![CDATA['.$actionUrls.']]></url_shelly_found>';
                $xml .= '<is_enabled>'.$isEnabled.'</is_enabled>';
                
                $xml .= '</'.$actionUrl.'>';
            }
            $xml .= '</index_'.$index.'>';
        }
        $xml .= '<all_success>'.$isAllSuccess.'</all_success>';
        $xml .= '</init>';

        $xml .= '</result>';
        sdk_header('text/xml');
        echo $xml;
    } else if($type != '') {
        // battery device thanks to report_url (action report values)
        switch ($type) {
            case 'dw':
            case 'dw2':
                // temp_p=12345&temp=24.10
                $temp_p = getArg('temp_p', false);
                if($temp_p != '') {
                    setValue($temp_p, getArg('temp', false), false, true);
                }
                
                // lux_p_id=12345&lux=130
                $lux_p = getArg('lux_p', false);
                if($lux_p != '') {
                    setValue($lux_p, getArg('lux', false), false, true);
                }
                
                // state_p=12345&state=open / close
                $state_p = getArg('state_p', false);
                if($state_p != '') {
                    $state_value = '0';
                    if (getArg('state', false) == 'open') {
                        $state_value = '100';
                    }
                    setValue($state_p, $state_value, false, true);
                }
                
                // vibration_p=12345&vibration=0 / 1
                $vibration_p = getArg('vibration_p', false);
                if($vibration_p != '') {
                    setValue($vibration_p, getArg('vibration', false) * 100, false, true);
                }
                
                //tilt_p=12345&tilt=0 / 1
                $tilt_p = getArg('tilt_p', false);
                if($tilt_p != '') {
                    setValue($tilt_p, getArg('tilt', false), false, true);
                }
                
            break;

            case 'flood':
                // temp_p=12345&temp=25.00
                $temp_p = getArg('temp_p', false);
                if($temp_p != '') {
                    setValue($temp_p, getArg('temp', false), false, true);
                }
                
                // bat_p_id=12345&batV=1.00
                $bat_p = getArg('bat_p', false);
                if($bat_p != '') {
                    setValue($bat_p, getArg('batV', false), false, true);
                }
                
                // flood_p=12345&flood=0 / 1
                $flood_p = getArg('flood_p', false);
                if($state_p != '') {
                    $state_value = '0';
                    if (getArg('flood', false) == 1) {
                        $state_value = '100';
                    }
                    setValue($state_p, $state_value, false, true);
                }

            break;

            case 'ht':
                // hum_p=1234&hum=60
                $hum_p = getArg('hum_p', false);
                if($hum_p != '') {
                    setValue($hum_p, getArg('hum', false), false, true);
                }
                
                // temp_p=12345&temp=25.00
                $temp_p = getArg('temp_p', false);
                if($temp_p != '') {
                    setValue($temp_p, getArg('temp', false), false, true);
                }
            break;

            default:
            break;
        }
               
        return;
    } else {
        //// UPDATE PERIPH ID list
        
        // example : &periphId=12345,678910
        $periphIdListToUpdate = explode(',', $periphId);

        // UPDATE from Shelly -> to Eedomus periphId
        foreach ($periphIdListToUpdate as $periphIdToUpdate) {
            // periph caract
            $periphIdCaractdUrl = 'http://localhost/api/get?action=periph.caract&periph_id='.$periphIdToUpdate.'&show_config=1&format=xml';
            $periphIdCaractdUrlXml = httpQuery($periphIdCaractdUrl);
            
            // patch '|' on xml node
            $periphIdCaractdUrlXml = str_replace('hide|', 'hide_', $periphIdCaractdUrlXml);
            
            $periphIdUrl = xpath($periphIdCaractdUrlXml,"//URL");
            
            // appel au status du shelly
            $periphIdUrlJson = httpQuery($periphIdUrl);
    
            $periphIdUrlXml = jsonToXML($periphIdUrlJson);
            
            $shellyStateValue = xpath($periphIdUrlXml, xpath($periphIdCaractdUrlXml,"//XPATH"));
            
            setValue($periphIdToUpdate, $shellyStateValue, false, true);
        }
    }
?>
