# shelly-plugin

## TL;DR
Permet de commander les modules [Shelly](https://shelly.cloud) avec une [Eedomus](https://www.eedomus.com/).



# Retour d'état

## API Gen1

Copier / coller [VAR3] dans un onglet d'un navigateur pour le retour d'état et remplacer les **%26** par **&**

Si tout est OK, en bas vous devez avoir <all_success>1</all_success>
-> Penser à faire Tester et valider sur les périphériques (bug Eedomus)

## API Gen2 (Gamme Plus et Pro)

Remplacer les **%26** par **&** dans le [VAR2] et [VAR3] sauf ceux après 'urls'




# Détails

### Dans le cas du choix d'un  __**Shelly Relay**__, ce plugin permet de créer 4 canaux:
** Pensez à  supprimer les canaux non utilisés ! **

[shelly1](https://www.shelly.cloud/products/shelly-1-smart-home-automation-relay/)

![shelly1](https://kb.shelly.cloud/__attachments/57049089/Shelly-1-front.jpg?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

[shelly1pm](https://www.shelly.cloud/products/shelly-1pm-smart-home-automation-relay/)

![shelly1pm](https://kb.shelly.cloud/__attachments/58032129/Shelly-1PM-front.jpg?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

[shelly1L](https://www.shelly.cloud/products/shelly-1l-single-wire-smart-home-automation-relay/)

![shelly1L](https://kb.shelly.cloud/__attachments/64651265/Shelly-1L.jpg?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

[shelly2-5](https://www.shelly.cloud/products/shelly-25-smart-home-automation-relay/)

![shelly2-5](https://kb.shelly.cloud/__attachments/65994767/Shelly-2.5-front.jpg?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

[shelly4pro](https://shelly-api-docs.shelly.cloud/gen1/#shelly4pro)

Plus disponible

![ShellyRelay](https://gitlab.com/eedomus/shelly-plugin/-/raw/develop/screenshot/relay.PNG)
- 3 [périphériques](https://doc.eedomus.com/view/Ajouter_un_p%C3%A9riph%C3%A9rique) :
  - 1 périphérique [Actionneur HTTP](https://doc.eedomus.com/view/Actionneurs_HTTP) pour __commander__ un canal (On/Off).
  - 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ instantané en Watt du canal.
  - 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ totale en Watt du canal.

### __**[Shelly H&T](https://shelly.cloud/products/shelly-humidity-temperature-smart-home-automation-sensor/)**__:

![ShellyH&T](https://kb.shelly.cloud/__attachments/64061685/image-20220920-070513.png?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

![ShellyH&T](https://gitlab.com/eedomus/shelly-plugin/-/raw/develop/screenshot/shellyH&T.PNG)
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __température__ en °C du Shelly H&T.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre l' __humidité__ en % du Shelly H&T.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre le __niveau de batterie__ en % du Shelly H&T.

### __**[Shelly Smoke](https://shelly.cloud/products/shelly-smoke-smart-home-automation-sensor/)**__:

Plus disponible

![ShellySmoke](https://gitlab.com/eedomus/shelly-plugin/-/raw/develop/screenshot/shelly_Smoke.PNG)
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre l' __état__ du Shelly Smoke OK/Alarme.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre l' __température__ en °C du Shelly Smoke.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre le __niveau de batterie__ en % du Shelly Smoke.

### __**[Shelly Dimmer 2](https://shelly.cloud/products/shelly-dimmer-2-smart-home-light-controller/)**__:

![ShellyPlug](https://kb.shelly.cloud/__attachments/63897613/image-20220920-071316.png?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

![ShellyDimmer](https://gitlab.com/eedomus/shelly-plugin/-/raw/develop/screenshot/shelly_dimmer.PNG)
- 1 périphérique [Actionneur HTTP](https://doc.eedomus.com/view/Actionneurs_HTTP) pour __commander__ la lumière (brightness) avec synchronisation de l'__état__.

### __**[Shelly Plug](https://shelly.cloud/products/shelly-plug-smart-home-automation-device/) / [Plug S](https://shelly.cloud/products/shelly-plug-s-smart-home-automation-device/)**__:

![ShellyPlug](https://kb.shelly.cloud/__attachments/64028673/image-20220920-070758.png?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

![ShellyPlugS](https://kb.shelly.cloud/__attachments/64061479/image-20220920-070723.png?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

- 1 périphérique [Actionneur HTTP](https://doc.eedomus.com/view/Actionneurs_HTTP) pour __commander__ la prise (On/Off) avec synchronisation de l'__état__.
 - 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ instantané en Watt du canal.
  - 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ totale en Watt du canal.

### __**[Addon température](https://shop.shelly.cloud/temperature-sensor-addon-for-shelly-1-1pm-wifi-smart-home-automation#312)**__

Plus disponible

![ShellyAddonTemp](https://gitlab.com/eedomus/shelly-plugin/-/raw/develop/screenshot/addon_temp.PNG)

- 3 périphériques [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaître la __température__ de la sonde 1, la __température__ de la sonde 2 et la __température__ de la sonde 3.

### __**[Shelly 2.5](https://shelly.cloud/products/shelly-25-smart-home-automation-relay/) en mode roller**__:

![Shelly25Roller](https://kb.shelly.cloud/__attachments/65994767/Shelly-2.5-front.jpg?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

![Shelly25Roller](https://gitlab.com/eedomus/shelly-plugin/-/raw/develop/screenshot/shelly_25_roller.PNG)
- 1 périphérique [Actionneur HTTP](https://doc.eedomus.com/view/Actionneurs_HTTP) pour __commander__ le roller avec synchronisation de l'__état__.

### __**[Shelly Flood](https://shelly.cloud/products/shelly-flood-smart-home-automation-sensor/)**__:

![ShellyFlood](https://kb.shelly.cloud/__attachments/64061714/image-20220920-070426.png?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

![ShellyFlood](https://gitlab.com/eedomus/shelly-plugin/-/raw/develop/screenshot/shelly_flood.PNG)
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre l' __état__ du Shelly Flood OK/Alarme.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre l' __température__ en °C du Shelly Smoke.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre le __niveau de batterie__ en % du Shelly Smoke.

### __**[Shelly Door/Window](https://shelly.cloud/products/shelly-door-window-2-smart-home-automation-sensor/)**__:

![ShellyDoorWindow](https://kb.shelly.cloud/__attachments/64127277/image-20220920-070350.png?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

![ShellyDoorWindow](https://gitlab.com/eedomus/shelly-plugin/-/raw/develop/screenshot/shelly_door_window.PNG)
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre l' __état__ du Shelly Door/Window -> Fermé/Ouvert.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __luminosité__ en lux du Shelly Door/Window.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre le __niveau de batterie__ en % du Shelly Door/Window.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre l' __inclinaison__ en ° du Shelly Door/Window.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre l' __état de la vibration__ Pas de vibration/Vibration du Shelly Door/Window.

### __**[Shelly EM](https://shelly.cloud/products/shelly-em-smart-home-automation-device/)**__:

![ShellyEM](https://kb.shelly.cloud/__attachments/64029019/image-20220920-072542.png?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

![ShellyEM](https://gitlab.com/eedomus/shelly-plugin/-/raw/develop/screenshot/shelly_EM.PNG)
- 1 périphérique [Actionneur HTTP](https://doc.eedomus.com/view/Actionneurs_HTTP) pour __commander__ un canal (On/Off).
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ instantané en Watt du canal.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ totale en Watt du canal.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre __puissance réactive__ instantané en Watt du canal.

### __**[Shelly 3EM](https://shelly.cloud/products/shelly-3em-smart-home-automation-energy-meter/)**__:

![Shelly3EM](https://kb.shelly.cloud/__attachments/63832224/image-20220920-071052.png?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

![Shelly3EM](https://gitlab.com/eedomus/shelly-plugin/-/raw/develop/screenshot/shelly_3EM.PNG)

- 1 périphérique [Actionneur HTTP](https://doc.eedomus.com/view/Actionneurs_HTTP) pour __commander__ un canal (On/Off).

Pour 3 canaux :
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ instantané en Watt du canal.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ totale en Watt du canal.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre le __facteur de puissnace__ du canal.

### __**[Shelly RGBW2](https://shelly.cloud/products/shelly-rgbw2-smart-home-automation-led-controller/) (color mode)**__:

![ShellyRGWBW2Color](https://kb.shelly.cloud/__attachments/63864967/image-20220920-071244.png?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

![ShellyRGWBW2Color](https://gitlab.com/eedomus/shelly-plugin/-/raw/develop/screenshot/shelly_RGBW2_color_mode.PNG)

- 1 périphérique [Actionneur HTTP](https://doc.eedomus.com/view/Actionneurs_HTTP) pour __commander__ la luminosité.
- 1 périphérique [Actionneur HTTP](https://doc.eedomus.com/view/Actionneurs_HTTP) pour __commander__ la couleur.
- 1 périphérique [Actionneur HTTP](https://doc.eedomus.com/view/Actionneurs_HTTP) pour __commander__ l'intensité du blanc.
- 1 périphérique [Actionneur HTTP](https://doc.eedomus.com/view/Actionneurs_HTTP) pour __commander__ une scène prédéfinie.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ instantané en Watt.

### __**[Shelly RGBW2](https://shelly.cloud/products/shelly-rgbw2-smart-home-automation-led-controller/) (white mode)**__, ce plugin permet de créer 4 canaux:
** Pensez à  supprimer les canaux non utilisés ! **

![ShellyRGWBW2White](https://kb.shelly.cloud/__attachments/63864967/image-20220920-071244.png?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

![ShellyRGWBW2White](https://gitlab.com/eedomus/shelly-plugin/-/raw/develop/screenshot/shelly_RGBW2_white_mode.PNG)

- 1 périphérique [Actionneur HTTP](https://doc.eedomus.com/view/Actionneurs_HTTP) pour __commander__ la luminosité.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ instantané en Watt.
- 1 périphérique [Capteur HTTP](https://doc.eedomus.com/view/Capteurs_HTTP) pour connaitre la __consommation__ totale en Watt.

### __**[Shelly i3](https://shelly.cloud/products/shelly-i3-smart-home-automation-device/)**__:

![Shelly i3](https://kb.shelly.cloud/__attachments/63832262/image-20220912-075931.png?inst-v=2004001f-f9ca-45f4-8fbc-cadf26d0a69a)

TODO faire le screenshot ...

TODO faire la description des périphériques ...


## Documentation
- Création d'un [plugin Eedomus](https://doc.eedomus.com/view/Le_store_eedomus)
- [Programmation des plugins Eedomus](https://docs.google.com/document/d/1j6Nd5LZ5Dou5g8pzK2zuFkeo3exunZxRvIQN2ZUaZ0s) de @Merguez07
- [API Shelly](https://shelly-api-docs.shelly.cloud/)
- [Lien vers le forum Eedomus](https://forum.eedomus.com/viewtopic.php?t=8920)

## Code Source
https://gitlab.com/eedomus/shelly-plugin


Enjoy (^_^)
